import socket
import threading
import sys
import os
import subprocess
from selectors import select
import fcntl
from shared import *

HOST = socket.gethostname()
PORT = 9000

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen(5)

print('Waiting for connections on: {}:{}'.format(HOST, PORT))


def start_shell():
    """Start a shell subprocess and returns a tuple (process, input, output)"""
    (pipe_in_r, pipe_in_w) = os.pipe()
    (pipe_out_r, pipe_out_w) = os.pipe()
    p = subprocess.Popen(['/bin/sh'], stdin=pipe_in_r, stdout = pipe_out_w, stderr = pipe_out_w)
    dest_w = os.fdopen(pipe_in_w, 'wb')
    dest_r = os.fdopen(pipe_out_r, 'rb')
    fd_set_nonblocking(pipe_out_r)
    return p, dest_r, dest_w


def client_thread(client_socket):
    host, port = client_socket.getsockname()
    print("New client: {}:{}".format(host, port))
    client_socket.setblocking(0) 
    p, shell_out, shell_in = start_shell()
    copy_loop(shell_out, shell_in, client_socket)
    print("Client ({}:{}) disconnected.".format(host, port))
    
while True:
    (client_socket, address) = server_socket.accept()
    thread = threading.Thread(target=client_thread, args=(client_socket,))
    thread.start()

