import socket
import sys
import os
from selectors import select
import fcntl

def fd_set_nonblocking(fd):
    oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
    return oldflags

def fd_set(fd, flags):
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)

def copy_loop(source_read, source_write, socket, optimal_buffer_size = 10240):
    """Copies data to and from socket and files. 
    Files must be opened in binary mode."""
    data_for_socket = bytes([])
    data_for_shell = bytes([])

    socket_alive = True

    while socket_alive:

        # block until either stdin or socket have some data to read
        data_sources = []
        data_dests = []
       
        if len(data_for_socket) < optimal_buffer_size:
            data_sources.append(source_read)
        if len(data_for_shell) < optimal_buffer_size:
            data_sources.append(socket)

        if len(data_for_socket) > 0:
            data_dests.append(socket)
        if len(data_for_shell) > 0:
            data_dests.append(source_write)

        readable, writeable, _ = select.select(data_sources, data_dests,[])

        # poll stdin for something to read if buffer has space
        if source_read in readable:
            # read bytes from stdin and write it to the socket
            data = source_read.read()
            data_for_socket = data_for_socket + data
        
        # poll socket for something to read if buffer has space
        if socket in readable:
            # read bytes from stdin and write it to the socket
            data = socket.recv(optimal_buffer_size)
            data_for_shell = data_for_shell + data

            if data == b'':
                socket_alive = False

        if socket in writeable:
            sent = socket.send(data_for_socket)
            data_for_socket = data_for_socket[sent:]

        if source_write in writeable:
            sent = source_write.write(data_for_shell)
            source_write.flush()
            data_for_shell = data_for_shell[sent:]