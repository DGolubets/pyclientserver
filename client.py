import socket
import threading
import sys
import fcntl
import termios
import os
from selectors import select
from shared import *

if len(sys.argv) < 2:
    sys.stderr.write("Expecting host:port")
    sys.exit()

host, port = sys.argv[1].split(':')
port = int(port)

# https://docs.python.org/2/faq/library.html#how-do-i-get-a-single-keypress-at-a-time
# get STDIN file descriptor
fd = sys.stdin.fileno()

# disable echo for STDIN and switch it to "noncanonical" mode to get input without waiting for a newline
# http://man7.org/linux/man-pages/man3/termios.3.html
oldterm = termios.tcgetattr(fd)
# newattr = termios.tcgetattr(fd)
# newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
# termios.tcsetattr(fd, termios.TCSANOW, newattr)

# set STDIN non-blocking mode
oldflags = fd_set_nonblocking(fd)

try:
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.connect((host, port))
    socket.setblocking(0) 

    copy_loop(sys.stdin.buffer, sys.stdout.buffer, socket)

finally:
    # restore terminal to the original state
    termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
    fd_set(fd, oldflags)

